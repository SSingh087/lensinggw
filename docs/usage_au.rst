Arbitrary units
===============
* `Example 2 - Solve the lens model (arbitrary units) <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/binary_point_mass_scaled_units.py>`__

| ``lensingGW`` supports arbitrary coordinate units, defined as :math:`x_{\mathrm{a.u.}} = x_{\mathrm{radians}}/ \alpha`, where :math:`\alpha` is a constant.
  To use arbitrary units, all input must be given in the scaled coordinate system and the **scaled** flag must be enabled in the
  routines involving coordinates or conversions to physical units.
  
| The `previous example <https://gpagano.gitlab.io/lensinggw/usage.html#radians>`_ can be modified to use :math:`\alpha = \theta_{E}`, where :math:`\theta_{E}` is the
  Einstein radius of the total mass:
  replace each quantity in radians with its scaled equivalent and modify the solver settings and the *TimeDelay* input as follows

.. code-block:: python
        
        # solver setup
        solver_settings = {'Scaled'           : True,             # indicate that the input is in scaled units 
                           'ScaleFactor'      : thetaE,           # and the scale factor  
                           'SearchWindowMacro': 4*thetaE1/thetaE, # scaled input
                           'SearchWindow'     : 4*thetaE2/thetaE} # scaled input

.. code-block:: python
 
        # time delays, magnifications, Morse indices
        tds = TimeDelay(Img_ra, Img_dec,                                                # already in scaled units
                        source_pos_x = 1.39e-11/thetaE, source_pos_y = 1.20e-10/thetaE, # scaled input
                        zL = 0.5 , zS = 2.0,
                        lens_model_list, kwargs_lens_list,
                        scaled       = solver_settings['Scaled'],      # indicate that the input is in scaled units 
                        scale_factor = solver_settings['ScaleFactor']) # and the scale factor   
                
The magnification and Morse indices computation doesn't need modifications, provided that all radians input has been scaled. This includes
the lens parameters and the source position.
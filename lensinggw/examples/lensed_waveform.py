# This script illustrates how to use lensingGW's lensed waveform routines
import numpy as np

######################
# lens configuration #
######################

# source position, in radians
beta0, beta1 = 1.38717585112603e-11, 1.2013295265914425e-10

# redshifts
zL, zS = 0.5, 2.0

# binary point mass lens model
lens_model_list     = ['POINT_MASS', 'POINT_MASS'] 
kwargs_point_mass_1 = {'center_x': 6.93587925563015e-11,'center_y': 0.0, 'theta_E': 9.808814510294364e-11} 
kwargs_point_mass_2 = {'center_x': -6.93587925563015e-11,'center_y': 0.0, 'theta_E': 9.808814510294364e-11} 
kwargs_lens_list    = [kwargs_point_mass_1, kwargs_point_mass_2]

# binary point mass images, in radians
ra  = np.array([2.06184855e-11,  6.74286421e-11, -8.55036309e-11])
dec = np.array([2.04174704e-10, -6.17971410e-11, -5.67605886e-11])

####################
# lensed waveforms #
####################

from lensinggw.waveform.waveform import gw_signal

# read the waveform parameters
config_file = 'ini_files/waveform_config.ini'

# instantiate the waveform model
waveform_model = gw_signal(config_file)

# compute the lensed waveform polarizations, strains in the requested detectors and their frequencies
freqs_lensed, hp_tilde_lensed, hc_tilde_lensed, lensed_strain_dict = waveform_model.lensed_gw(ra,dec,
                                                                                              beta0,beta1,
                                                                                              zL, zS,
                                                                                              lens_model_list,
                                                                                              kwargs_lens_list)

# and their signal-to-noise-ratios
lensed_SNR_dict = waveform_model.lensed_snr(ra,dec,
                                            beta0,beta1,
                                            zL, zS,
                                            lens_model_list,
                                            kwargs_lens_list)

# access a lensed strain
lensed_sH1 = lensed_strain_dict['H1']